kde-gtk-config (4:6.3.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.3.1).
  * New upstream release (6.3.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 28 Feb 2025 00:52:57 +0100

kde-gtk-config (4:6.3.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.3.0).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 10 Feb 2025 14:59:36 +0100

kde-gtk-config (4:6.2.91-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.91).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 23 Jan 2025 23:52:42 +0100

kde-gtk-config (4:6.2.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.90).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 11 Jan 2025 23:38:44 +0100

kde-gtk-config (4:6.2.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.5).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 05 Jan 2025 10:59:31 +0100

kde-gtk-config (4:6.2.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 03 Dec 2024 16:36:07 +0100

kde-gtk-config (4:6.2.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.3).
  * Update the list of installed files.
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 23 Nov 2024 21:57:30 +0100

kde-gtk-config (4:6.2.1-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.2.1).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 15 Oct 2024 18:21:00 +0200

kde-gtk-config (4:6.2.0-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.90).
  * Update build-deps and deps with the info from cmake.
  * New upstream release (6.2.0).
  * Tighten inter-Plasma package dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 05 Oct 2024 23:17:46 +0200

kde-gtk-config (4:6.1.5-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.5).
  * Tighten Plasma packages inter-dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 11 Sep 2024 23:27:59 +0200

kde-gtk-config (4:6.1.4-2) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * Tighten Plasma packages inter-dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 18 Aug 2024 00:51:01 +0200

kde-gtk-config (4:6.1.4-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 11 Aug 2024 23:58:22 +0200

kde-gtk-config (4:6.1.3-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (6.1.3).

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 21 Jul 2024 23:46:17 +0200

kde-gtk-config (4:6.1.0-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (6.1.0).
  * Update build-deps and deps with the info from cmake.
  * Update list of installed files.

 -- Patrick Franz <deltaone@debian.org>  Thu, 20 Jun 2024 00:04:14 +0200

kde-gtk-config (4:5.27.11-1) unstable; urgency=medium

  [ Patrick Franz ]
  * Bump Standards-Version to 4.7.0 (No changes needed).
  * New upstream release (5.27.11).
  * Update build-deps and deps with the info from cmake.
  * Remove inactive uploaders, thanks for your work.

 -- Patrick Franz <deltaone@debian.org>  Sun, 19 May 2024 12:21:21 +0200

kde-gtk-config (4:5.27.10-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.10).
  * Update build-deps and deps with the info from cmake.

 -- Patrick Franz <deltaone@debian.org>  Thu, 11 Jan 2024 23:25:07 +0100

kde-gtk-config (4:5.27.9-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.9).
  * Update build-deps and deps with the info from cmake.

 -- Patrick Franz <deltaone@debian.org>  Fri, 27 Oct 2023 21:53:48 +0200

kde-gtk-config (4:5.27.8-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.8).

 -- Patrick Franz <deltaone@debian.org>  Wed, 13 Sep 2023 21:53:45 +0200

kde-gtk-config (4:5.27.7-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.7).
  * Update build-deps and deps with the info from cmake.

 -- Patrick Franz <deltaone@debian.org>  Thu, 03 Aug 2023 18:54:35 +0200

kde-gtk-config (4:5.27.5-2) unstable; urgency=medium

  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 27 May 2023 18:23:45 +0200

kde-gtk-config (4:5.27.5-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.5).

 -- Patrick Franz <deltaone@debian.org>  Tue, 09 May 2023 23:27:42 +0200

kde-gtk-config (4:5.27.3-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.27.3).

 -- Patrick Franz <deltaone@debian.org>  Wed, 22 Mar 2023 23:17:52 +0100

kde-gtk-config (4:5.27.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.27.1).
  * New upstream release (5.27.2).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 28 Feb 2023 15:00:42 +0100

kde-gtk-config (4:5.27.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.27.0).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 18 Feb 2023 17:08:43 +0100

kde-gtk-config (4:5.26.90-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.90).
  * Bump Standards-Version to 4.6.2, no change required.
  * Update cross-plasma versioned dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 23 Jan 2023 21:50:56 +0100

kde-gtk-config (4:5.26.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Sat, 07 Jan 2023 00:21:54 +0100

kde-gtk-config (4:5.26.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 29 Nov 2022 16:05:51 +0100

kde-gtk-config (4:5.26.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.3).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 08 Nov 2022 15:45:59 +0100

kde-gtk-config (4:5.26.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.1).
  * New upstream release (5.26.2).
  * Add gsettings-desktop-schemas as a dependency. (Closes: #1023372)

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 27 Oct 2022 23:34:57 +0200

kde-gtk-config (4:5.26.0-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.26.0).
  * Release to unstable.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 11 Oct 2022 15:44:21 +0200

kde-gtk-config (4:5.25.90-2) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * Tighten cross-plasma components dependencies.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 07 Oct 2022 11:47:04 +0200

kde-gtk-config (4:5.25.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.90).
  * Update build-deps and deps with the info from cmake.

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 25 Sep 2022 00:14:24 +0200

kde-gtk-config (4:5.25.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 09 Sep 2022 23:21:51 +0200

kde-gtk-config (4:5.25.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 02 Aug 2022 17:33:46 +0200

kde-gtk-config (4:5.25.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.25.2).
  * New upstream release (5.25.3).
  * Refresh lintian overrides.
  * Release to unstable

 -- Aurélien COUDERC <coucouf@debian.org>  Sun, 17 Jul 2022 15:28:43 +0200

kde-gtk-config (4:5.25.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.25.1).
  * Tighten inter-Plasma dependencies.

 -- Patrick Franz <deltaone@debian.org>  Tue, 21 Jun 2022 21:47:07 +0200

kde-gtk-config (4:5.25.0-1) experimental; urgency=medium

  * New upstream release (5.25.0).
  * Tighten cross-plasma components dependencies.
  * Bump Standards-Version to 4.6.1, no change required.

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 14 Jun 2022 21:26:45 +0200

kde-gtk-config (4:5.24.90-1) experimental; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.90).
  * Drop obsolete lintian override.

 -- Aurélien COUDERC <coucouf@debian.org>  Fri, 20 May 2022 11:45:38 +0200

kde-gtk-config (4:5.24.5-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.5).

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 12 May 2022 21:40:11 +0200

kde-gtk-config (4:5.24.4-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.4).

 -- Aurélien COUDERC <coucouf@debian.org>  Wed, 30 Mar 2022 14:03:03 +0200

kde-gtk-config (4:5.24.3-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * New upstream release (5.24.3).
  * Added myself to the uploaders.

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 10 Mar 2022 08:19:13 +0100

kde-gtk-config (4:5.24.2-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.24.2).
  * Update B-Ds and deps with the info from cmake.
  * Update list of installed files.
  * Re-export signing key without extra signatures.

 -- Patrick Franz <deltaone@debian.org>  Sat, 26 Feb 2022 21:58:20 +0100

kde-gtk-config (4:5.23.5-1) unstable; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.23.5).
  * Update B-Ds.

 -- Patrick Franz <deltaone@debian.org>  Fri, 07 Jan 2022 17:22:13 +0100

kde-gtk-config (4:5.23.4-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.4).

  [ Patrick Franz ]
  * Update B-Ds.

 -- Patrick Franz <deltaone@debian.org>  Thu, 02 Dec 2021 21:58:42 +0100

kde-gtk-config (4:5.23.3-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.3).

 -- Norbert Preining <norbert@preining.info>  Wed, 10 Nov 2021 08:37:40 +0900

kde-gtk-config (4:5.23.2-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.1).
  * New upstream release (5.23.2).

 -- Norbert Preining <norbert@preining.info>  Wed, 03 Nov 2021 22:21:02 +0900

kde-gtk-config (4:5.23.0-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Update upstream signing-key.
  * Tighten build-dependencies.
  * Bump Standards-Version to 4.6.0 (no changes needed).

 -- Patrick Franz <deltaone@debian.org>  Thu, 14 Oct 2021 21:08:22 +0200

kde-gtk-config (4:5.23.0-1) unstable; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.23.0).
  * Update list of installed files.

 -- Norbert Preining <norbert@preining.info>  Thu, 14 Oct 2021 20:13:14 +0900

kde-gtk-config (4:5.21.5-2) unstable; urgency=medium

  [ Patrick Franz ]
  * Release to unstable.

 -- Patrick Franz <deltaone@debian.org>  Tue, 17 Aug 2021 03:23:21 +0200

kde-gtk-config (4:5.21.5-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.21.5).

 -- Patrick Franz <patfra71@gmail.com>  Fri, 07 May 2021 18:04:28 +0200

kde-gtk-config (4:5.21.4-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.21.4).

 -- Patrick Franz <patfra71@gmail.com>  Tue, 06 Apr 2021 17:46:29 +0200

kde-gtk-config (4:5.21.3-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.21.3).

 -- Norbert Preining <norbert@preining.info>  Wed, 17 Mar 2021 05:49:41 +0900

kde-gtk-config (4:5.21.2-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release (5.21.2).

 -- Norbert Preining <norbert@preining.info>  Wed, 03 Mar 2021 05:33:49 +0900

kde-gtk-config (4:5.21.1-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.21.1).

 -- Norbert Preining <norbert@preining.info>  Wed, 24 Feb 2021 14:36:39 +0900

kde-gtk-config (4:5.21.0-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.21.0).
  * Remove upstream included patch.
  * Bump B-D for libkdecorations2-dev.

  [ Aurélien COUDERC ]
  * Add lintian override for libwindow-decorations-gtk-module.so not linking
    against libc, it only uses glib/gio symbols.
  * Refresh copyright information.

 -- Norbert Preining <norbert@preining.info>  Wed, 17 Feb 2021 05:42:26 +0900

kde-gtk-config (4:5.20.5-2) unstable; urgency=medium

  * Stop echoing debug messages to stdout (Closes: #979540)

 -- Norbert Preining <norbert@preining.info>  Fri, 08 Jan 2021 07:37:55 +0900

kde-gtk-config (4:5.20.5-1) unstable; urgency=medium

  * New upstream release (5.20.5).
  * Fix list of installed files.

 -- Norbert Preining <norbert@preining.info>  Wed, 06 Jan 2021 23:50:51 +0900

kde-gtk-config (4:5.20.4-3) unstable; urgency=medium

  * Recommend xsettingsd with alternative xsettings-kde instead of
    xsettings-kde only (thanks to Rik Mills)

 -- Norbert Preining <norbert@preining.info>  Mon, 28 Dec 2020 09:47:09 +0900

kde-gtk-config (4:5.20.4-2) unstable; urgency=medium

  [ Scarlett Moore ]
  * Add missing runtime dependency for xsettings-kde from cmake.
  * Add override for libexec lintian as due to upstream defaulting
    to this location and the fragile link to helpers it must remain.
    - kde-config-gtk-style - gtk_theme
    - kde-config-gtk-style-preview - gtk3_preview
  * Bump standard version to 4.5.1; no changes required.
  * Refresh copyright.
    - Update to upstream move to spdx format.
    - Remove obsolete entries.
    - Update new entries for all files.
    - Fix syntax on kdeev entry.

  [ Norbert Preining ]
  * Release to unstable.

 -- Norbert Preining <norbert@preining.info>  Tue, 22 Dec 2020 11:04:36 +0900

kde-gtk-config (4:5.20.4-1) experimental; urgency=medium

  [ Norbert Preining ]
  * New upstream release (5.20.4).
  * Update list of installed files.

  [ Scarlett Moore ]
  * Add versioning to the kf5/qt5/plasma build dependencies.

 -- Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>  Wed, 11 Nov 2020 12:53:33 +0900

kde-gtk-config (4:5.19.5-3) unstable; urgency=medium

  [ Scarlett Moore ]
  * Remove unneeded --asneeded ldflag as it is default now.
  * Remove obsolete entries in d/upstream/metadata already
    defined in d/copyright.

 -- Norbert Preining <norbert@preining.info>  Fri, 06 Nov 2020 08:37:22 +0900

kde-gtk-config (4:5.19.5-2) experimental; urgency=medium

  * Rebuild for Qt 5.15

 -- Norbert Preining <norbert@preining.info>  Mon, 02 Nov 2020 09:27:39 +0900

kde-gtk-config (4:5.19.5-1) experimental; urgency=medium

  * New upstream release (5.19.5).
  * Add Patrick and myself to uploaders.

 -- Norbert Preining <norbert@preining.info>  Mon, 19 Oct 2020 10:33:45 +0900

kde-gtk-config (4:5.19.4-1) experimental; urgency=medium

  * Team upload.

  [ Scarlett Moore ]
  * Bump compat level to 13.
  * Add Rules-Requires-Root field to control.
  * New upstream release (5.18.5).
  * Update build-deps and deps with the info from cmake.
  * Update Homepage link to point to new invent.kde.org
  * Update field Source in debian/copyright to invent.kde.org move.
  * Add myself to Uploaders.
  * Set/Update field Upstream-Contact in debian/copyright.
  * Update build-deps and deps with the info from cmake.

  [ Patrick Franz ]
  * Update files in debian/kde-config-gtk-style.install.
  * Update matching files in debian/copyright.
  * Update data in /debian/upstream/metadata.
  * Update field Source in debian/copyright.
  * Update Homepage in debian/control.
  * New upstream release (5.19.4).
  * Update build-deps and deps with the info from cmake.
  * Change Maintainer in debian/control to Debian Qt/KDE Maintainers.
  * Remove Maxy from Uploaders as requested.
  * Update debian/kde-config-gtk-style.install.
  * Add Lintian-override library-not-linked-against-libc for
    libcolorreload-gtk-module.so:
    - Library only uses glib/gio APIs resulting in no NEEDED on libc.

 -- Norbert Preining <norbert@preining.info>  Fri, 16 Oct 2020 10:28:55 +0900

kde-gtk-config (4:5.17.5-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * Bump Standards-Version to 4.5.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Fri, 14 Feb 2020 21:36:32 +0100

kde-gtk-config (4:5.17.5-1) experimental; urgency=medium

  * Team upload.

  [ Maximiliano Curia ]
  * New upstream release (5.16.5).
  * Salsa CI automatic initialization by Tuco
  * Update build-deps and deps with the info from cmake

  [ Pino Toscano ]
  * New upstream release.
  * Explicitly add the gettext build dependency.
  * Bump the debhelper compatibility to 12:
    - switch the debhelper build dependency to debhelper-compat 12
    - remove debian/compat
  * Drop the 'testsuite' autopkgtest, as it does not test the installed
    packages.
  * Update install files.
  * Handle conffiles removal.
  * Stop passing -DDATA_INSTALL_DIR to cmake, as it is no more needed.
  * Bump Standards-Version to 4.4.1, no changes required.

 -- Pino Toscano <pino@debian.org>  Sat, 18 Jan 2020 21:59:35 +0100

kde-gtk-config (4:5.14.5-1) unstable; urgency=medium

  * New upstream release (5.14.5).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Thu, 24 Jan 2019 09:25:44 -0300

kde-gtk-config (4:5.14.3-1) unstable; urgency=medium

  * Update upsteam signing-key
  * New upstream release (5.14.3).
  * Update build-deps and deps with the info from cmake
  * Rediff patches
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Fri, 23 Nov 2018 08:50:28 -0300

kde-gtk-config (4:5.13.5-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.2.1 (was 4.1.4): no changes required.
  * Update long descriptions of binary packages for fixing errors found by
    lintian: description-contains-invalid-control-statement.
  * Simplify debian/control: drop requirement of specific version of cmake
    (was >= 2.8.12), because even Debian Jessie has cmake version 3.0.2.
  * Simplify debian/rules: stop adding of CPPFLAGS into CXXFLAGS using of
    DEB_CXXFLAGS_MAINT_APPEND as workaround for cmake because debhelper
    already does this since version 9.20120417.
  * Add patch 0001-Fix-spelling-errors.
  * Tiny cosmetic changes in debian/watch.
  * Update debian/copyright.

 -- Boris Pek <tehnick@debian.org>  Sun, 23 Sep 2018 23:40:51 +0300

kde-gtk-config (4:5.13.4-1) unstable; urgency=medium

  * New upstream release (5.13.4).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Sun, 19 Aug 2018 23:17:54 +0200

kde-gtk-config (4:5.13.1-1) unstable; urgency=medium

  * New upstream release (5.13.1).
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Tue, 26 Jun 2018 13:42:52 +0200

kde-gtk-config (4:5.12.5-1) unstable; urgency=medium

  * New upstream release (5.12.2).
  * New upstream release (5.12.5).
  * Bump Standards-Version to 4.1.4.
  * Use https for the debian/copyright
  * Release to unstable

 -- Maximiliano Curia <maxy@debian.org>  Wed, 09 May 2018 13:23:54 +0200

kde-gtk-config (4:5.12.1-1) sid; urgency=medium

  * Use the salsa canonical urls
  * New upstream release (5.12.1).
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Tue, 20 Feb 2018 22:08:42 +0100

kde-gtk-config (4:5.12.0-2) sid; urgency=medium

  * New revision
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Mon, 12 Feb 2018 16:03:29 +0100

kde-gtk-config (4:5.12.0-1) experimental; urgency=medium

  * Bump debhelper build-dep and compat to 11.
  * Build without build_stamp
  * Add Bhushan Shah upstream signing key
  * New upstream release (5.12.0).
  * Bump Standards-Version to 4.1.3.
  * Use https uri for uscan
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Thu, 08 Feb 2018 15:20:31 +0100

kde-gtk-config (4:5.11.4-1) experimental; urgency=medium

  * New upstream release (5.11.4).
  * Bump Standards-Version to 4.1.2.
  * Drop removed xdg file
  * Release to experimental

 -- Maximiliano Curia <maxy@debian.org>  Wed, 03 Jan 2018 16:48:48 -0300

kde-gtk-config (4:5.10.5-2) sid; urgency=medium

  * New revision
  * Bump Standards-Version to 4.1.0.
  * Release to sid

 -- Maximiliano Curia <maxy@debian.org>  Sun, 03 Sep 2017 09:55:16 +0200

kde-gtk-config (4:5.10.5-1) experimental; urgency=medium

  [ Maximiliano Curia ]
  * Update build-deps and deps with the info from cmake
  * Update upstream metadata
  * Update build-deps and deps with the info from cmake
  * New upstream release (5.10.5).
  * Release to experimental

  [ Jonathan Riddell ]
  * update watch file to version=4 with pgp signature checking

 -- Maximiliano Curia <maxy@debian.org>  Mon, 28 Aug 2017 15:28:11 +0200

kde-gtk-config (4:5.10.4-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.0.0 (was 3.9.8): no changes required.
  * Bump debhelper version to 10 (was 9); update debian/compat.
  * Update debian/control:
    - simpify Build-Depends and sort them using wrap-and-sort tool
    - set Multi-Arch fields to binary packages
  * Update debian/rules:
    - set DEB_CXXFLAGS_MAINT_APPEND, DEB_LDFLAGS_MAINT_APPEND and
      DEB_BUILD_MAINT_OPTIONS variables
    - set DATA_INSTALL_DIR variable in configuration options: it is required
      for correct search of preview.ui file in gtk*_preview programs.
      (These programs have not been working since version 4:5.1.95-0ubuntu1)
  * Add patch fix-search-of-gtk-preview-executables.
    It is required for showing preview buttons in KDE-GTK-config UI.
    (These buttons have not been working since version 4:5.1.95-0ubuntu1)
  * Update debian/copyright.

 -- Boris Pek <tehnick@debian.org>  Fri, 04 Aug 2017 16:59:19 +0300

kde-gtk-config (4:5.8.6-1) unstable; urgency=medium

  * New upstream release (5.8.6)

 -- Maximiliano Curia <maxy@debian.org>  Tue, 14 Mar 2017 15:23:29 +0100

kde-gtk-config (4:5.8.5-1) experimental; urgency=medium

  * New upstream release (5.8.5).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 30 Dec 2016 18:46:13 +0100

kde-gtk-config (4:5.8.4-1) unstable; urgency=medium

  * New upstream release (5.8.4)

 -- Maximiliano Curia <maxy@debian.org>  Wed, 23 Nov 2016 18:35:00 +0100

kde-gtk-config (4:5.8.2-1) unstable; urgency=medium

  * New upstream release (5.8.2)

 -- Maximiliano Curia <maxy@debian.org>  Wed, 19 Oct 2016 15:19:42 +0200

kde-gtk-config (4:5.8.1-1) unstable; urgency=medium

  * New upstream release (5.8.1)

 -- Maximiliano Curia <maxy@debian.org>  Sun, 16 Oct 2016 22:57:06 +0200

kde-gtk-config (4:5.8.0-1) unstable; urgency=medium

  [ Maximiliano Curia ]
  * New upstream release (5.8.0)
  * Replace dbus-launch with dbus-run-session in tests

  [ Automatic packaging ]
  * Update build-deps and deps with the info from cmake

 -- Maximiliano Curia <maxy@debian.org>  Fri, 07 Oct 2016 14:02:16 +0200

kde-gtk-config (4:5.7.4-1) unstable; urgency=medium

  * New upstream release (5.7.4)

 -- Maximiliano Curia <maxy@debian.org>  Fri, 26 Aug 2016 14:47:25 +0200

kde-gtk-config (4:5.7.0-1) unstable; urgency=medium

  * New upstream release.

 -- Maximiliano Curia <maxy@debian.org>  Sat, 09 Jul 2016 22:13:23 +0200

kde-gtk-config (4:5.6.5-1) unstable; urgency=medium

  * New upstream release.

 -- Maximiliano Curia <maxy@debian.org>  Thu, 23 Jun 2016 09:41:49 +0200

kde-gtk-config (4:5.6.4-1) unstable; urgency=medium

  [ Maximiliano Curia ]
  * uscan no longer supports this kind of watch files.
  * New upstream release (5.5.5).
  * Add upstream metadata (DEP-12)
  * debian/control: Update Vcs-Browser and Vcs-Git fields

  [ Automatic packaging ]
  * Bump Standards-Version to 3.9.8

 -- Maximiliano Curia <maxy@debian.org>  Thu, 26 May 2016 19:14:42 +0200

kde-gtk-config (4:5.5.4-1) experimental; urgency=medium

  * New upstream release (5.5.0).
  * New upstream release (5.5.1).
  * New upstream release (5.5.2).
  * New upstream release (5.5.3).
  * New upstream release (5.5.4).

 -- Maximiliano Curia <maxy@debian.org>  Wed, 27 Jan 2016 16:49:10 +0100

kde-gtk-config (4:5.4.3-1) unstable; urgency=medium

  * New upstream release (5.4.3).

 -- Maximiliano Curia <maxy@debian.org>  Tue, 01 Dec 2015 11:46:02 +0100

kde-gtk-config (4:5.4.2-1) unstable; urgency=medium

  * New upstream release (5.4.2).

 -- Maximiliano Curia <maxy@debian.org>  Tue, 06 Oct 2015 07:52:18 +0200

kde-gtk-config (4:5.4.1-1) unstable; urgency=medium

  * New upstream release (5.4.1).

 -- Maximiliano Curia <maxy@debian.org>  Fri, 11 Sep 2015 18:45:34 +0200

kde-gtk-config (4:5.4.0-1) unstable; urgency=medium

  * New upstream release (5.4.0).

 -- Maximiliano Curia <maxy@debian.org>  Thu, 03 Sep 2015 13:20:05 +0200

kde-gtk-config (4:5.3.2-1) unstable; urgency=medium

  * New upstream release (5.3.2).

 -- Maximiliano Curia <maxy@debian.org>  Tue, 30 Jun 2015 17:34:58 +0200

kde-gtk-config (4:5.3.1-0ubuntu2) wily; urgency=medium

  * debian/tests/control: Dep on fonts-oxygen: ttf-oxygen-font-family was
    dropped.

 -- Iain Lane <iain@orangesquash.org.uk>  Wed, 10 Jun 2015 12:27:32 +0100

kde-gtk-config (4:5.3.1-0ubuntu1) wily; urgency=medium

  * new upstream release

 -- Jonathan Riddell <jriddell@ubuntu.com>  Fri, 05 Jun 2015 02:26:22 +0200

kde-gtk-config (4:5.3.1-1) unstable; urgency=medium

  * New upstream release (5.3.0).
  * New upstream release (5.3.1).

 -- Maximiliano Curia <maxy@debian.org>  Mon, 29 Jun 2015 23:01:04 +0200

kde-gtk-config (4:5.2.2-1) experimental; urgency=medium

  * New upstream release (5.2.1).
  * New patch: fix_fonthelper
  * New upstream release (5.2.2).
  * Remove patch: fix_fonthelper

 -- Maximiliano Curia <maxy@debian.org>  Wed, 25 Mar 2015 23:17:51 +0100

kde-gtk-config (4:5.2.2-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Tue, 24 Mar 2015 07:12:23 -0700

kde-gtk-config (4:5.2.1-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Scarlett Clark <sgclark@kubuntu.org>  Mon, 23 Feb 2015 09:36:51 -0800

kde-gtk-config (4:5.2.0-1) experimental; urgency=medium

  * Prepare initial Debian release.
  * Add myself as Uploader.
  * Update build dependencies to build against experimental and to
    follow cmake.
  * Update copyright information.
  * Update install files.

 -- Maximiliano Curia <maxy@debian.org>  Mon, 09 Feb 2015 00:40:29 +0100

kde-gtk-config (4:5.2.0-0ubuntu1) vivid; urgency=medium

  * New upstream release

 -- Harald Sitter <sitter@kde.org>  Tue, 27 Jan 2015 14:50:24 +0100

kde-gtk-config (4:5.1.95-0ubuntu1) vivid; urgency=medium

  * New upstream release
  * Bump dh compat to 9
  * Switch to autpkgtest since tests require fonts.
  * Drop all patches, no longer applicable due to kf5 port.

 -- Harald Sitter <sitter@kde.org>  Thu, 15 Jan 2015 01:21:22 +0100

kde-gtk-config (3:2.2.1-1) unstable; urgency=low

  * Update to stable release 2.2.1.
  * Move package to unstable: no changes required.
  * Update debian/*.install due to changes in upstream sources.
  * Delete debian/patches/check-gtk-preview-files-in-runtime:
    now available in upstream.
  * Delete files:
    - debian/gtk3_preview.1
    - debian/gtk_preview.1
    - debian/kde-config-gtk-style-preview.manpages
    - debian/reload_gtk_apps.1
    these binary files were moved from /usr/bin to /usr/lib/kde4
    so manpages are not required anymore.
  * Rename debian/patches/mirgation-from-package-src:kcm-gtk
    to debian/patches/migration-from-package-src:kcm-gtk and
    update it. (Thanks to Ralf Jung)
  * Package kde-config-gtk-style now suggests package
    kde-config-gtk-style-preview (instead of recommends)
    because now binary file reload_gtk_apps is in package
    kde-config-gtk-style.
  * Add debian/patches/fix-loading-config-options:
    fix loading of config options. (Closes: #708169)
  * Add debian/patches/fix-desktop-file:
    - fix errors in desktop file
    - use icon name gtkconfig instead of kde-gtk-config
  * Update debian/rules:
    rename icon kde-gtk-config.png to gtkconfig.png
  * Update Vcs-* fields in debian/control:
    git repo was moved from GitHub to Alioth.
  * Update debian/copyright.
  * Update debian/watch.

 -- Boris Pek <tehnick-8@mail.ru>  Thu, 23 May 2013 02:05:43 +0300

kde-gtk-config (3:2.1.1-1) experimental; urgency=low

  * Update project Homepage.
  * Update debian/control for kde-config-gtk-style-preview:
    - add Breaks against old kde-config-gtk-style (Closes: #698285)
    - add Recommends against kde-config-gtk-style
  * Bump Standards-Version to 3.9.4 (was 3.9.3): no changes required.

 -- Boris Pek <tehnick-8@mail.ru>  Sun, 20 Jan 2013 15:21:10 +0200

kde-gtk-config (3:2.1.1-1~exp1) experimental; urgency=low

  * Update to stable release 2.1.1.
  * Delete debian/patches/fix-loading-icons: now available in upstream.
  * Add debian/patches/check-gtk-preview-files-in-runtime:
    check if binary files for previewing Gtk themes are available.
  * Update debian/control:
    - add package kde-config-gtk-style-preview
    - update long description of package kde-config-gtk-style
    - kde-config-gtk-style recommends kde-config-gtk-style-preview
  * Rename debian/manpages to debian/kde-config-gtk-style-preview.manpages.
  * Add files debian/*.install: see above.
  * Fix get-orig-source section in according with Debian Policy §4.9.
  * Small fix in debian/watch.

 -- Boris Pek <tehnick-8@mail.ru>  Wed, 07 Nov 2012 00:35:58 +0200

kde-gtk-config (3:2.1-1) unstable; urgency=low

  * Update to stable release 2.1.
  * Deleted file debian/patches/fix-build-on-kfreebsd-and-hurd-i386:
    accepted in upstream.
  * Added file debian/patches/fix-loading-icons:
    without this patch KDE-GTK-Config module scans ./ directory if option
    gtk-icon-theme-name or option gtk-fallback-icon-theme is empty or invalid,
    now first element from the list of found icon themes is used.
    (Closes: #678714)

 -- Boris Pek <tehnick-8@mail.ru>  Tue, 21 Aug 2012 00:22:23 +0300

kde-gtk-config (3:2.0-3) unstable; urgency=low

  * Package moved to unstable after tests: no changes required.
  * This package replaces src:kcm-gtk and closes its bugs:
    - fixed working on clean install (Closes: #631591)
    - added support of Gtk+ 3.x (LP: #734979)

 -- Boris Pek <tehnick-8@mail.ru>  Fri, 08 Jun 2012 07:06:54 +0300

kde-gtk-config (3:2.0-2) experimental; urgency=low

  * Added file debian/patches/fix-build-on-kfreebsd-and-hurd-i386:
    fixes FTBFS in Debian GNU/kFreeBSD and in Debian GNU/Hurd.

 -- Boris Pek <tehnick-8@mail.ru>  Wed, 06 Jun 2012 19:50:31 +0300

kde-gtk-config (3:2.0-1) experimental; urgency=low

  * Initial release (closes: #659103).

 -- Boris Pek <tehnick-8@mail.ru>  Tue, 13 Mar 2012 06:57:17 +0200
